/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pencamaster.models.Fixture;
import pencamaster.models.Forecast;
import pencamaster.models.Group;
import pencamaster.models.Match;
import pencamaster.models.Rules;
import pencamaster.models.User;

/**
 *
 * @author Mathias
 */
public class LoadServiceTest {
    
    public LoadServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of testCalculateMatchPoints method, of class LoadService.
     */
    @Test
    public void testCalculateMatchPoints() {
        System.out.println("calculateMatchPoints");
        Rules rules = new Rules();
        rules.setGroupsExactResultPoints(3);
        rules.setGroupsResultPoints(1);
        int expResult = 1;
        
        //Exact result
        int result = LoadService.calculateMatchPoints(2, 2, 3, 3);
        assertEquals(expResult, result);
        
        //Result ok but not exact
        expResult = 3;
        result = LoadService.calculateMatchPoints(2, 2, 2, 2);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of testCalculateFinalMatchPoints method, of class LoadService.
     */
    @Test
    public void testCalculateFinalMatchPoints() {
        System.out.println("calculateFinalMatchPoints");
        Rules.finalResultPoints = 2;
        int expResult = 2;
        
        //Exact result
        int result = LoadService.calculateFinalPhasePoints(3, 1, 1, 0);
        assertEquals(expResult, result);
        
        //Result ok but not exact
        expResult = 0;
        result = LoadService.calculateFinalPhasePoints(2,0, 0, 2);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of sortUsersByPoints method, of class LoadService.
     */
    @Test
    public void testSortUsersByPoints() {
        System.out.println("sortUsersByPoints");
        User u1 = new User("u1");
        u1.setTotalScore(10);
        User u2 = new User("u2");
        u2.setTotalScore(3);
        User u3 = new User("u3");
        u3.setTotalScore(7);
        LinkedHashMap<String,User> users = new LinkedHashMap();
        users.put(u1.getNickName(), u1);
        users.put(u2.getNickName(), u2);
        users.put(u3.getNickName(), u3);
        Group.users = users;
        ArrayList<User> result = LoadService.sortUsersByPoints();
        assertEquals(3, result.size());
        assertEquals((Integer) 3, result.get(0).getTotalScore());
        assertEquals((Integer) 7, result.get(1).getTotalScore());
        assertEquals((Integer) 10, result.get(2).getTotalScore());
    }
    
    @Test
    public void testLoadPencaPoints() {
        Rules.groupsExactResultPoints = 3;
        Rules.groupsResultPoints = 1;
        //Create Match 1
        LinkedList<Match> matches = new LinkedList();
        Match match = new Match("1","Uruguay","Argentina",null,"A","Grupos");
        match.setLocalFinalScore(1);
        match.setVisitantFinalScore(0);
        //Create Match 2
        Match match2 = new Match("2","Uruguay","Peru",null,"B","Grupos");
        match2.setLocalFinalScore(2);
        match2.setVisitantFinalScore(1);
        matches.add(match);
        matches.add(match2);
        Fixture.matches = matches;
        //Create Prediction
        Forecast pred = new Forecast(match,1,0);
        Forecast pred2 = new Forecast(match2,1,0);
        
        //Create User
        User u1 = new User("u1");
        u1.addPrediction("1", pred);
        u1.addPrediction("2", pred2);
        LinkedHashMap<String,User> users = new LinkedHashMap();
        users.put(u1.getNickName(), u1);
        Group.users = users;
        
        //Call function
        LoadService.loadPencaPoints();
        assertEquals((Integer) 4,Group.users.get("u1").getTotalScore());
    }
    
}
