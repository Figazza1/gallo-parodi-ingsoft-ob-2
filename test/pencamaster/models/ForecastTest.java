/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mathias
 */
public class ForecastTest {
    
    public ForecastTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLocalScore method, of class Forecast.
     */
    @Test
    public void testGetLocalScore() {
        System.out.println("getLocalScore");
        Forecast instance = new Forecast(null,1,2);
        int expResult = 1;
        int result = instance.getLocalScore();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLocalScore method, of class Forecast.
     */
    @Test
    public void testSetLocalScore() {
        System.out.println("setLocalScore");
        int localScore = 0;
        Forecast instance = new Forecast(null,1,2);
        instance.setLocalScore(localScore);
        assertEquals(instance.localScore, 0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getVisitantScore method, of class Forecast.
     */
    @Test
    public void testGetVisitantScore() {
        System.out.println("getVisitantScore");
        Forecast instance = new Forecast(null,1,2);
        int expResult = 2;
        int result = instance.getVisitantScore();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setVisitantScore method, of class Forecast.
     */
    @Test
    public void testSetVisitantScore() {
        System.out.println("setVisitantScore");
        int visitantScore = 0;
        Forecast instance = new Forecast(null,1,2);
        instance.setVisitantScore(visitantScore);
        assertEquals(instance.visitantScore, 0);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
