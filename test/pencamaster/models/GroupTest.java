/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mparodi
 */
public class GroupTest {
    
    public GroupTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of AddUser method, of class Group.
     */
    @Test
    public void testAddUser() throws Exception {
        System.out.println("AddUser");
        String userNick = "u1";
        User userObject = new User("u1");
        Group.AddUser(userNick, userObject);
        assertEquals(Group.users.get("u1"),userObject);
    }
    
}
