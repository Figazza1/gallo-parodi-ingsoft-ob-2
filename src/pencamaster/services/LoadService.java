package pencamaster.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import pencamaster.models.Fixture;
import pencamaster.models.Forecast;
import pencamaster.models.Group;
import pencamaster.models.Match;
import pencamaster.models.Rules;
import pencamaster.models.User;

public class LoadService {
    
    public static void loadFixture(){
        
    }
    
    public static void loadMatchResults(){
        
    }
    
    public static void loadPencaPoints(){
        System.out.println("Calculando puntos");
        LinkedHashMap<String,User> users = Group.users;
        Set<String> keys = users.keySet(); 
        for (String k: keys){
            // get user
            User user = users.get(k);
            Integer points = 0;
            // get his predictions
            LinkedHashMap<String,Forecast> userPredictions = user.getPredictions();
            
            //iterate over fixture matches
            LinkedList<Match> matches = Fixture.matches;
            for (int i = 0; i < matches.size(); i++){
                Match match = matches.get(i);
                if(userPredictions.containsKey(match.getId())){
                    // check if the match has final scores
                    Forecast pred = userPredictions.get(match.getId());
                    if (match.getLocalFinalScore() != null && match.getVisitantFinalScore() != null){
                        if (match.getPhase().equals("Grupos")){
                            points = points + calculateMatchPoints(match.getLocalFinalScore(),match.getVisitantFinalScore(),pred.getLocalScore(),pred.getVisitantScore());
                        }else{
                            points = points + calculateFinalPhasePoints(match.getLocalFinalScore(),match.getVisitantFinalScore(),pred.getLocalScore(),pred.getVisitantScore());
                        }
                        
                    }
                }
            }
            user.setTotalScore(points);
        }
    }
    
    public static Integer calculateMatchPoints(Integer localResult, Integer visitResult, Integer localPred, Integer visitPred){
        Rules rules = Rules.getInstance();
        // Case exact result
        if (localResult.equals(localPred) && visitResult.equals(visitPred)){
            return rules.getGroupsExactResultPoints();
        } else if ((localResult > visitResult && localPred > visitPred) || (localResult < visitResult && localPred < visitPred) || (localResult == visitResult && localPred == visitPred)){
            return rules.getGroupsResultPoints();
        }
        return 0;
    }
    
    /**
     * Returns an array of users ordered from less points to more.
     */
    public static ArrayList<User> sortUsersByPoints() {
        ArrayList<User> positions = new ArrayList();
        LinkedHashMap<String,User> users = Group.users;
        Set<String> keys = users.keySet(); 
        for (String k: keys){
            positions.add(users.get(k));
        }
        UsersComparator c = new UsersComparator();
        Collections.sort(positions, c);
        return positions;
    }

    /**
     * Returns points to be asigned to a match from octavos to final
     */
    public static Integer calculateFinalPhasePoints(Integer localResult, Integer visitResult, Integer localPred, Integer visitPred){
        if ((localResult > visitResult && localPred > visitPred) || (localResult < visitResult && localPred < visitPred) || (localResult == visitResult && localPred == visitPred)){
            return Rules.finalResultPoints;
        }
        return 0;
    }
    
    /**
     * Comparator class used for sortUsersbYPoints method.
     */
    public static class UsersComparator implements Comparator<User> {
        @Override
        public int compare(User u1, User u2) {
            return u1.getTotalScore().compareTo(u2.getTotalScore());
        }
    }  
}
