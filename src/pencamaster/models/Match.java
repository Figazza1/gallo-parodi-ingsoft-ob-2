/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import java.util.Date;

/**
 *
 * @author Mathias
 */
public class Match {
    String id;
    String localTeam;
    String visitantTeam;
    Date date;
    String group;
    String phase;
    Boolean wasPlayed;
    
    // Final Result
    Integer localFinalScore;
    Integer visitantFinalScore;

    public Match(String id, String localTeam, String visitantTeam, Date date, String group, String phase) {
        this.id = id;
        this.localTeam = localTeam;
        this.visitantTeam = visitantTeam;
        this.date = date;
        this.group = group;
        this.phase = phase;
        this.localFinalScore = null;
        this.visitantFinalScore = null;
        this.wasPlayed = false;
    }

    public String getId() {
        return id;
    }

    public String getLocalTeam() {
        return localTeam;
    }

    public String getVisitantTeam() {
        return visitantTeam;
    }

    public Date getDate() {
        return date;
    }

    public String getGroup() {
        return group;
    }

    public String getPhase() {
        return phase;
    }

    public Integer getLocalFinalScore() {
        return localFinalScore;
    }

    public Integer getVisitantFinalScore() {
        return visitantFinalScore;
    }

    public void setLocalFinalScore(Integer localFinalScore) {
        this.localFinalScore = localFinalScore;
    }

    public void setVisitantFinalScore(Integer visitantFinalScore) {
        this.visitantFinalScore = visitantFinalScore;
    }
    
    public Boolean wasPlayed(){
        return this.wasPlayed;
    }
    
    
}
