package pencamaster.models;

public class Rules {
    static Rules instance = null;
    public static int groupsExactResultPoints = 0;
    public static int groupsResultPoints = 0;
    public static int finalResultPoints = 0;
    
    public static Rules getInstance(){ 
        if(instance == null){ 
            instance = new Rules(); 
        }
        return instance; 
    }

    public int getGroupsExactResultPoints() {
        return groupsExactResultPoints;
    }

    public void setGroupsExactResultPoints(int groupsExactResultPoints) {
        this.groupsExactResultPoints = groupsExactResultPoints;
    }

    public int getGroupsResultPoints() {
        return groupsResultPoints;
    }

    public void setGroupsResultPoints(int groupsResultPoints) {
        this.groupsResultPoints = groupsResultPoints;
    }

    public int getFinalResultPoints() {
        return finalResultPoints;
    }

    public void setFinalResultPoints(int finalResultPoints) {
        this.finalResultPoints = finalResultPoints;
    }
    
}
