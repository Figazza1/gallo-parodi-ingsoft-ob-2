/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *
 * @author Mathias
 */
public class User {
    String nickName;
    LinkedHashMap<String,Forecast> predictions;
    Integer totalScore;

    public User(String nickName) {
        this.nickName = nickName;
        this.predictions = null;
        this.totalScore = 0;
        this.predictions = new LinkedHashMap();
    }
    
    public void addPrediction(String matchId, Forecast prediction){
        this.predictions.put(matchId, prediction);
    }

    public LinkedHashMap<String, Forecast> getPredictions() {
        return predictions;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public String getNickName() {
        return nickName;
    }
    
    
}
