/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import java.util.Objects;

/**
 *
 * @author Mathias
 */
public class Forecast {
    int localScore;
    int visitantScore;

    public Forecast(Match match, int localScore, int visitantScore) {
        this.localScore = localScore;
        this.visitantScore = visitantScore;
    }

    public int getLocalScore() {
        return localScore;
    }

    public void setLocalScore(int localScore) {
        this.localScore = localScore;
    }

    public int getVisitantScore() {
        return visitantScore;
    }

    public void setVisitantScore(int visitantScore) {
        this.visitantScore = visitantScore;
    }
   
    
}
