/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster.models;

import java.util.LinkedList;

/**
 *
 * @author Mathias
 */
public class Fixture {
    public static LinkedList<Match> matches = new LinkedList();
    
    public static void AddMatch(Match matchObject) throws Exception {
        if (matches.contains(matchObject)){
            throw new Exception("Partido ya existente");
        } else{
           matches.add(matchObject); 
        }
    }
    
    public static void AddResultToMatch(String matchId, Integer localScore, Integer visitScore){
        for(int i = 0; i < matches.size(); i++){
            Match match = matches.get(i);
            if(match.id.equals(matchId)){
                match.setLocalFinalScore(localScore);
                match.setVisitantFinalScore(visitScore);
            }
        }
    }
}
