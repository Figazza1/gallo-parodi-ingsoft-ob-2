/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pencamaster;

import com.opencsv.CSVReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import pencamaster.models.Fixture;
import pencamaster.models.Forecast;
import pencamaster.models.Group;
import pencamaster.models.Match;
import pencamaster.models.User;
import pencamaster.services.LoadService;

/**
 *
 * @author Mathias
 */
public class IndividualGroupPhaseView extends javax.swing.JFrame {

    /**
     * Creates new form DashboardPanel
     */
    public IndividualGroupPhaseView() {
        initComponents();
        setDefaultCloseOperation(IndividualGroupPhaseView.DISPOSE_ON_CLOSE);
    }
    
    static IndividualGroupPhaseView instance = null;
    public static IndividualGroupPhaseView getInstance(){ 
        if(instance == null){ 
        instance = new IndividualGroupPhaseView(); 
        }
        return instance; 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Local", "Visitante", "Fecha", "Goles Local", "Goles Visitante"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jButton1.setText("Hacer predicción");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Completa los goles de local y visitante. Asegurese de que no quedaron casillas ");

        jLabel2.setText("seleccionadas antes de confirmar sus predicciones.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int tableIndex = 0;
        for (int i = 0; i < Fixture.matches.size(); i++){
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            Date now = cal.getTime();
            Match match = Fixture.matches.get(i); 
            System.out.println("Fixture iterando sobre " + match.getLocalTeam() + " vs " + match.getVisitantTeam());
            if (match.getDate().after(now) && match.getPhase().equals("Grupos")){
              System.out.println("Intentando predecir  " +  this.jTable1.getValueAt(tableIndex, 3) + " - " + this.jTable1.getValueAt(tableIndex, 4));
                try{
                    if (match.getDate().after(now) && match.getPhase().equals("Grupos")){
                        try{
                        Integer localScore = (Integer) this.jTable1.getValueAt(tableIndex, 3);
                        Integer visitScore = (Integer) this.jTable1.getValueAt(tableIndex, 4);
                        System.out.println("score predicted is" + localScore + " - "+ visitScore);
                        String userNick = GeneralView.selectedUser;
                        Forecast prediction = new Forecast(match,localScore,visitScore);
                        User user = Group.users.get(userNick);
                        user.addPrediction(match.getId(), prediction);
                        System.out.println("Added prediction");
                        }
                        catch(Exception e){
                            System.out.println("NO SE PUDO PREDECIR EL MATCH CON ID " + match.getId());
                            System.out.println(e.getMessage());
                        }
                    }else{
                        System.out.println("El partido de " + match.getLocalTeam() + " vs " + match.getVisitantTeam() + " ya ocurrio por lo que no se puede predecir");
                    }
                }
                catch(Exception e){
                    System.out.println("error");
                    System.out.println(e.getStackTrace());
                } 
                tableIndex++;
            } else {
                System.out.println("Partido ya jugado ");
            }
            
        }
        LoadService.loadPencaPoints();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IndividualGroupPhaseView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IndividualGroupPhaseView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IndividualGroupPhaseView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IndividualGroupPhaseView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IndividualGroupPhaseView().setVisible(true);
            }
        });
    }
    
    private void cleanTable(){
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int rowCount = model.getRowCount();
            //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
    
    public void loadFixture(){
        cleanTable();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date now = cal.getTime();
        try{
            LinkedList<Match> fixture = Fixture.matches;
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            String userNick = GeneralView.selectedUser;
            User user = Group.users.get(userNick);
            for (int i = 0; i< fixture.size(); i++){
                Match match = fixture.get(i);
                if (match.getDate().after(now) && match.getPhase().equals("Grupos")){ //If is not played show, else hide
                    try{
                    String local = match.getLocalTeam();
                    String visit = match.getVisitantTeam();
                    String date = sdf.format(match.getDate());
                    
                    Forecast prediction = user.getPredictions().get(match.getId());
                    if (prediction != null){
                        Integer localPred = prediction.getLocalScore();
                        Integer visitPred = prediction.getVisitantScore();
                        model.addRow(new Object[]{local,visit,date,localPred,visitPred});
                        System.out.println("Cargando resultado ya cargado");
                    } else{
                        System.out.println("Cargando resultado no cargado");
                        model.addRow(new Object[]{local,visit,date,"X","X"}); 
                    }
                    }catch(Exception e){
                        System.out.println(e.getMessage());
                    }
                }
                
            }
        }
        catch(Exception e){
            System.out.println("Error al leer el archivo csv");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
